package co.za.investec.utils;

import co.za.investec.model.Address;
import co.za.investec.model.AddressLineDetail;
import co.za.investec.model.Country;

public class AddressValidator {

    public static final String SOUTH_AFRICA = "ZA";

    public static boolean isValid(Address address) {

        //A valid address must consist of a numeric postal code, a country, and at least one address line that is not blank or null. If the country is ZA, then a province is required as well.
        return (isNumeric(address.getPostalCode()) && hasCountryDetail(address.getCountry())
                && hasAddressDetails(address.getAddressLineDetail()) && hasProvinceDetail(address));
        }


    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            System.out.println("Invalid Address - postal Code must be numeric");
            return false;
        }
        return true;
    }

    public static boolean hasAddressDetails(AddressLineDetail addressLineDetail) {

        if(addressLineDetail == null){
            return false;

        }else {
            if(addressLineDetail.getLine1().trim().isEmpty() &&
                    addressLineDetail.getLine2().trim().isEmpty()) {
                System.out.println("Invalid Address - at least one address line must be provided");
                return false;
            }

            return true;
        }


    }


    public static boolean hasProvinceDetail(Address address) {

        if(address.getCountry().getCode().equalsIgnoreCase(SOUTH_AFRICA)){
            if(address.getProvinceOrState() == null){
                System.out.println("Invalid Address - province is required for South African Address");
                return false;
            }
        }

        return true;
    }

    public static boolean hasCountryDetail(Country country) {

        if(country == null){
                System.out.println("Invalid Address - country is mandatory");
                return false;
        }

        return true;
    }

}
