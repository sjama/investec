package co.za.investec.utils;

import co.za.investec.model.Address;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public class JsonUtility {

    public static void main(String[] args) throws Exception  {

        JsonUtility jsonUtility = new JsonUtility();
        List<Address> addresses = (List<Address>) jsonUtility.getAddressListFromFile("addresses.json");
        jsonUtility.prettyPrintAllAddresses(addresses);
        jsonUtility.prettyPrintAddressesByType(AddressType.POSTAL_ADDRESS.getCode(),addresses );
        jsonUtility.validateAddresses(addresses);

    }

    private Collection<Address> getAddressListFromFile(String fileName) throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()));
        Gson gson = new Gson();
        Type collectionType = new TypeToken<Collection<Address>>(){}.getType();
        Collection<Address> addressList = gson.fromJson(bufferedReader, collectionType);
        return addressList;

    }


    public void validateAddresses(List<Address> addressList){

        for(Address address : addressList){
            AddressValidator.isValid(address);
        }

    }

    public void prettyPrintAllAddresses(List<Address> addressList){

        for(Address address : addressList){
            System.out.println(prettyPrintAddress(address));
        }

    }


    public void prettyPrintAddressesByType(String addressTypeCode , List<Address> addressList){

        for(Address address : addressList){
            if(addressTypeCode.equalsIgnoreCase(address.getType().getCode()))
            System.out.println(prettyPrintAddress(address));
        }

    }


    public String prettyPrintAddress(Address address) {

        StringBuilder sb = new StringBuilder();
        sb.append("Address Details \n");
        sb.append(" Type : " + address.getType().getName() + "\n");
        sb.append(" Line Details :" + "\n");
        if(address.getAddressLineDetail() != null) {
            sb.append("  line 1 : " + address.getAddressLineDetail().getLine1() + "\n");
            sb.append("  line 2 : " + address.getAddressLineDetail().getLine2() + "\n");
        }
        sb.append(" City : " + address.getCityOrTown()+ "\n");
        if(address.getProvinceOrState() != null)
        sb.append(" Province : " +
                address.getProvinceOrState().getName() + "\n");
        sb.append(" Postal Code : " + address.getPostalCode()+ "\n");
        sb.append(" Country : " + address.getCountry().getName()+ "\n");

        return sb.toString();


    }

    public enum AddressType{
        PHYSICAL_ADDRESS("1"),
        POSTAL_ADDRESS("2"),
        BUSINESS_ADDRESS("3");

        AddressType(String code){
            this.code = code;

        }

        private String code;
        public String getCode() {
            return code;
        }

    }
}
