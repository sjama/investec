package co.za.investec.utils;

public class NumberUtility {
    // Function to return gcd of a and b
    static int gcd(int a, int b)
    {
        if (a == 0)
            return b;
        return gcd(b % a, a);
    }

    // Function to find gcd of array of
    // numbers
    static int findGCD(int arr[], int n)
    {
        int result = arr[0];
        for (int i = 1; i < n; i++)
            result = gcd(arr[i], result);

        return result;
    }

    public static void main(String[] args)
    {
        int arr[] = { 120, 200,440 };
        NumberUtility gcd = new NumberUtility();

        gcd.highestCommonFactor(arr);

    }

    public int highestCommonFactor(int[] numbers) {
        int n = numbers.length;

        System.out.println(findGCD(numbers, n));

        return (findGCD(numbers, n));

    }
}